import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Artur on 24.10.2015.
 */
@ApplicationPath("resources")
public class App extends Application {
}
