package service;

import model.Person;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Artur on 24.10.2015.
 */
@Singleton
@Lock(LockType.READ)
public class PersonProvider {

    public List<Person> findAll() {
        return Arrays.asList(new Person(0, "Arti"), new Person(1, "Smarti"));
    }
}
