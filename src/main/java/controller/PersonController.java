package controller;

import model.Person;
import service.PersonProvider;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Artur on 24.10.2015.
 */
@Singleton
@Lock(LockType.READ)
@Path("person")
@Consumes("application/json")
@Produces("application/json")
public class PersonController {

    private PersonProvider personProvider = new PersonProvider();

    @GET
    @Path("{id: \\d+}")
    public Response getPersonWithId(@PathParam("id") final int id) {
        List<Person> persons = personProvider.findAll();
        return Response.ok(persons.get(id)).build();
    }
}
